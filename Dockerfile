FROM ubuntu:latest

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y \
	qtmultimedia5-dev \
	qtcreator \
	build-essential \
	qtbase5-dev \
	qt5-qmake

WORKDIR /Users/anabolovic/Documents/PomocniAzrs/azrscalculator/calculator

COPY . .

#COPY .qmake.stash /Users/anabolovic/Documents/PomocniAzrs/azrscalculator/calculator/
COPY . /Users/anabolovic/Documents/PomocniAzrs/

RUN qmake

RUN make

CMD ["./calculator"]
